
#include <EEPROM.h>

//uncommented when compiling for ATTiny (disables serial)
#define ATTINY

//uncommented when enable aiming mode
// (makes sketch too big for attiny24)
#define ENABLE_AIMING

#ifdef ATTINY
#define PRINTLN(s) 
#define PRINT(s)

//Inputs
#define PHOTO_PIN A5
#define MODE_SW_PIN 4

//Outputs
#define RELAY_PIN 0
#define LASER_PIN 6
#define LED_1 1
#define LED_2 2
#define LED_3 3

#else

//For testing on an Arduino Uno
#define PRINTLN(s) Serial.println(s);
#define PRINT(s) Serial.print(s);

//Inputs
#define PHOTO_PIN A5
#define MODE_SW_PIN 4

//Outputs
#define RELAY_PIN 7
#define LASER_PIN 6
#define LED_1 8
#define LED_2 2
#define LED_3 3

#endif

//Milliseconds for a activating relay in timed mode
#define ACTIVATION_DUR 5000

//Milliseconds for smoothing toggle mode activation
#define TOGGLE_DEBOUNCE_MS 250

//Milliseconds for holding down mode button to save current mode to eeprom
#define MODE_SET_BUTTON_HOLD_MS 1000

//Constants
//# of cycles for calibration
#define CALIB_COUNT 10
#define CALIB_DELAY 50

//how long to turn off/on mode pin for feedback blink
#define BLINK_INTERVAL 100


void setup() {
  pinMode(MODE_SW_PIN, INPUT);
  pinMode(PHOTO_PIN, INPUT);

  pinMode(RELAY_PIN, OUTPUT);
  pinMode(LASER_PIN, OUTPUT);
  pinMode(LED_1, OUTPUT);
  pinMode(LED_2, OUTPUT);
  pinMode(LED_3, OUTPUT);

  #ifndef ATTINY
  Serial.begin(9600);
  #endif

  #ifdef ENABLE_AIMING
  if(digitalRead(MODE_SW_PIN) == HIGH) {
    aimingMode(); 
  }
  #endif
  
  calibratePhotoSensor();

  byte savedMode = EEPROM.read(0);

  if(savedMode >= 0 && savedMode < 3) {
    setMode(savedMode);
  }
  else {
    setMode(0);
  }
}


/*******************\
|* Button Debounce *|
\*******************/

unsigned long lastDebounceTime = 0;
#define DEBOUNCE_DELAY 50 
int buttonState = LOW;
int lastButtonState = LOW;
bool getButtonState() {
  int reading = digitalRead(MODE_SW_PIN);
  
  if (reading != lastButtonState) {
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > DEBOUNCE_DELAY) {
    if (reading != buttonState) {
      buttonState = reading;
    }
  }

  lastButtonState = reading;
  return buttonState == HIGH;
}

bool wasPressed = false;
unsigned long lastDown = 0;

bool buttonPressed() {
  bool pressedNow = getButtonState();
  if(pressedNow && !wasPressed) {
    lastDown = millis();
    wasPressed = true;
    return false;
  }
  else if(!pressedNow && wasPressed) {
    //Button up
    wasPressed = false;
    lastDown = 0;
    return true;
  }
  else {
    wasPressed = pressedNow;
    return false;
  }
}

bool checkProlongedPress() {
  if(lastDown > 0 && millis() - lastDown >= MODE_SET_BUTTON_HOLD_MS) {
    lastDown = 0;
    return true;
  }
  return false;
}

//Wait for button to be released, then absorb the "button-up" event
void awaitButtonRelease() {
  while(getButtonState()) {
    delay(5);
  }
  wasPressed = false;
}


/*******************************\
|* sensor input
\********************************/

int blockThresh; //When PHOTO_PIN give value below this threshold, we're blocked


bool checkBlockage() {
  //Read PHOTO_PIN and return true if value indicates beam is blocked
  return (analogRead(PHOTO_PIN) > blockThresh);
}


void calibratePhotoSensor() {
  //Turn the laser on and off and compare the readings to figure out 
  // optimal threshold value for detecting when beam is blocked
  int on = 0, off = 0;
  for(int i=0; i < CALIB_COUNT; i++) {
    digitalWrite(LASER_PIN, LOW);
    delay(CALIB_DELAY);
    PRINT("BLOCKED:");
    PRINTLN(analogRead(PHOTO_PIN));
    off += analogRead(PHOTO_PIN);
    digitalWrite(LASER_PIN, HIGH);
    delay(CALIB_DELAY);
    PRINT("SHINE:");
    PRINTLN(analogRead(PHOTO_PIN));
    on += analogRead(PHOTO_PIN);
  }
  blockThresh = ((off-on)/2 + on)/CALIB_COUNT;
  PRINTLN(blockThresh);
}




/*******************************\
|* LEDS 
\********************************/

int ledPins[] = {LED_1, LED_2, LED_3};

void setModeLEDs(byte mode) {
  for(int i=0; i < 3; i++) {
    digitalWrite(ledPins[i], mode == i ? HIGH : LOW);
  }
}

void blinkLed(byte mode) {

  for(byte i=0; i < 3; i++) {
    digitalWrite(ledPins[mode], LOW);
    delay(BLINK_INTERVAL);
    digitalWrite(ledPins[mode], HIGH);
    if(i != 2) delay(BLINK_INTERVAL);
  }
}




/*******************************\
|* Timer
\********************************/


unsigned long nextTimer;

void setTimer(unsigned long durationMillis) {
  PRINT("setting timer ");
  PRINTLN(durationMillis);
  nextTimer = millis()+durationMillis;
}
void cancelTimer() {
  nextTimer = 0;
}
bool isTimerExpired() {
  return (nextTimer != 0 && millis() >= nextTimer);
}

/*******
 * Mode Implementations
 *******/

class Mode {
  public:
  virtual void blockageChange(bool blocked);
  virtual void timerExpired();
};


/*
 * ModeA: Correlated
 * relay status matches blocked status
 */
class CorrelateMode : public Mode {
  public:
  void blockageChange(bool blocked) {
    digitalWrite(RELAY_PIN, blocked ? HIGH : LOW);
  }
  void timerExpired() {}
};

/*
 * ModeB: Toggle
 * when blockage is detected, toggle status of relay
 */

class ToggleMode : public Mode {
  private:
    bool relayActive = false;
  public:
  void blockageChange(bool blocked) {
    if(blocked) {
      this->relayActive = !(this->relayActive);
      digitalWrite(RELAY_PIN, this->relayActive ? HIGH : LOW);

      //disable the laser for a few millis to absorb unintended fluctuations
      digitalWrite(LASER_PIN, LOW);
      setTimer(TOGGLE_DEBOUNCE_MS);
    }
  }
  void timerExpired() {
    digitalWrite(LASER_PIN, HIGH);
  }
};

/*
 * ModeC: Timed
 * when blockage is detected, activate for a time period
 */

class TimedMode : public Mode {
  private:
    bool relayActive = false;
  public:
  void blockageChange(bool blocked) {
    PRINT("blocked");
    PRINTLN(blocked);
    if(blocked && !this->relayActive) {
      digitalWrite(RELAY_PIN, HIGH);
      digitalWrite(LASER_PIN, LOW);
      setTimer(ACTIVATION_DUR);
    }
  }
  void timerExpired() {
    digitalWrite(RELAY_PIN, LOW);
    digitalWrite(LASER_PIN, HIGH);
    this->relayActive = false;  
  }
};


CorrelateMode modeA;
ToggleMode modeB;
TimedMode modeC;

Mode* modeImpls[] = {&modeA, &modeB, &modeC};

byte currMode = 0;
Mode* currModeImpl;

void setMode(byte modeNum) {
  currMode = modeNum;
  cancelTimer();
  digitalWrite(LASER_PIN, HIGH);
  digitalWrite(RELAY_PIN, LOW);
  currModeImpl = modeImpls[currMode];
  setModeLEDs(currMode);
}

void nextMode() {
  setMode((currMode + 1)%3);
}


bool lastBlocked = false;

void loop() {
  
  if(buttonPressed()) {
    nextMode();
  }

  if(checkProlongedPress()) {
    blinkLed(currMode);
    EEPROM.write(0, currMode);
    awaitButtonRelease();
  }

  bool currBlocked = checkBlockage();
  if(currBlocked != lastBlocked) {
    currModeImpl->blockageChange(currBlocked);
  }
  lastBlocked = currBlocked;

  if(isTimerExpired()) {
    PRINTLN("Timer Expired");
    cancelTimer();
    currModeImpl->timerExpired();
  }
  
  delay(5);
}


#ifdef ENABLE_AIMING

unsigned long nextBlink = 0;
bool blinkState = false;
byte currLed = 0;
void doBlinks() {
  if(!blinkState || millis() > nextBlink) {
    for(byte i=0; i < 3; i++) {
      digitalWrite(ledPins[i], (i==currLed) ? HIGH : LOW);
    }
    currLed = (currLed+1)%3;
    nextBlink = millis() + BLINK_INTERVAL;
  }
  blinkState = true;
}

void allLedsOn() {
  blinkState = false;
  for(byte i=0; i < 3; i++) {
    digitalWrite(ledPins[i], HIGH);
  }
}


void aimingMode() {

  //dark PHOTO_PIN -> higher analogRead
  int minVal; //PHOTO_PIN value when illuminated
  int maxVal;
  
  //Read ambient light value (no illumination)
  digitalWrite(LASER_PIN, LOW);
  maxVal = analogRead(PHOTO_PIN);
  minVal = maxVal - 40;

  //Enable laser, and stream in minVal/maxVals, /w periodic pulse
  // every second, turn off led for 100 millis and grab dark value
  digitalWrite(LASER_PIN, HIGH);
  int thresh = 0, currVal;
  bool okayButtonCheck = false, buttonStatus; //to wait until button released before checking status
  
  while(1) {

    buttonStatus = (digitalRead(MODE_SW_PIN) == HIGH);
    if(!buttonStatus) {
      okayButtonCheck = true;
    }
    
    currVal = analogRead(PHOTO_PIN);
  
    if(currVal < minVal) {
      minVal = currVal;
    }
    if(currVal > maxVal) {
      maxVal = currVal;
    }

    thresh = (maxVal - minVal)/2 + minVal;

    if(currVal < thresh) {
      allLedsOn();
      if(okayButtonCheck && buttonStatus) {
        break;
      }
    }
    else {
      doBlinks();
    }

    delay(10);
  }

  
}
#endif
