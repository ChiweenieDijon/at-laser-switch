# AT Laser Switch

 Plans and source code for an Arduino/AtTiny based project for a low-cost Assistive Tech laser beam switch device.  Includes circuitboard and a 3D-printed case mounted to a PVC frame.

Contents: 

*KiCad* - Schematic and PCB layout for the circuitboard 

*laserswitch_AtTiny24_84* - Arduino sketch for the microcontroller

*STL* - 3d-printable files for the case parts


Notes: 

The PCB has been fabricated and tested and it works.  

The design uses an Arduino sketch written to an AtTiny24 microcontroller, which can be done using a standard Arduino Uno as a programmer. (It's pretty straightforward, just google it!)

If you want to build this, drop me an email:  gene (at) noonian.org
